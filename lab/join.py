# -*- coding: utf-8 -*-

import json
import unittest

from rsmr import MapReduce


def map(item):
    k = item[1]
    v = [item[0]] + item[2:]
    yield k,v


def reduce(key, values):
    wiersze = values
    liczbawierszy = len(wiersze)
    tmp = 0
    tabpom = []
    pom = []
    for pom in wiersze:
        if pom[0] == 'order':
            for pe in wiersze:
                if pe[0] == 'line_item':
                    tabpom.append([key]+pom+pe)
    return tabpom


def read_data(filename):
    data = open(filename)
    return json.load(data)


def join_records():
    input_data = read_data('records.json')
    mapper = MapReduce(map, reduce)
    results = mapper(input_data)
    return results


class JoinTest(unittest.TestCase):

    def test_results(self):
        expected = sorted(read_data('join.json'))
        current = sorted(join_records())
        self.assertListEqual(expected, current)


if __name__ == '__main__':
    unittest.main()
