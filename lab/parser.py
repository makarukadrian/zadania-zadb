import sys
import apachelog


def parse(filename):
    format = r'%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"'
    parser = apachelog.parser(format)

    with open(filename, 'r') as file:
        for line in file:
            try:
                row = parser.parse(line)
                row['%t'] = row['%t'][1:12]+' '+row['%t'][13:21]+' '+row['%t'][22:27]
                yield row
            except apachelog.ApacheLogParserError:
                sys.stderr.write("Problem z parsowaniem %s" % line)

