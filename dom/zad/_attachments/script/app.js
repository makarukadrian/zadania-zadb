// Apache 2.0 J Chris Anderson 2011
$(function() {   
    // friendly helper http://tinyurl.com/6aow6yn
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    $('#przycisk').click(function(){
        var ilosc = parseInt($("#ilosc").val());
        drawItems(ilosc);
    });

    var path = unescape(document.location.pathname).split('/'),
        design = path[3],
        db = $.couch.db(path[1]);
    function drawItems(maks) {
        db.view(design + "/widok_1", {
            descending : "true",
            group: true,
            update_seq : true,
            success : function(data) {
                setupChanges(data.update_seq);
                var them = $.mustache($("#widok_1").html(), {
                    lista1 : data.rows.map(function(r) {r.value['adres'] = r.key ;return r.value;}).sort(
                        function(a, b) {
                            return a.ilosc - b.ilosc;}).reverse().slice(0, maks),
                    lista2 : data.rows.map(function(r) {r.value['adres'] = r.key ;return r.value;}).sort(
                        function(a, b) {
                            return a.suma - b.suma;}).reverse().slice(0, maks)
                });
                $("#content1").html(them);
            }
        });
        db.view(design + "/widok_2", {
            descending : "true",
            group: true,
            update_seq : true,
            success : function(data) {
                setupChanges(data.update_seq);
                var them = $.mustache($("#widok_2").html(), {
                    lista1 : data.rows.map(function(r) {r.value['adres'] = r.key ;return r.value;}).sort(
                        function(a, b) {
                            return a.ilosc - b.ilosc;}).reverse().slice(0, maks),
                    lista2 : data.rows.map(function(r) {r.value['adres'] = r.key ;return r.value;}).sort(
                        function(a, b) {
                            return a.suma - b.suma;}).reverse().slice(0, maks)
                });
                $("#content2").html(them);
            }
        });
    };
    drawItems(0);
    var changesRunning = false;
    function setupChanges(since) {
        if (!changesRunning) {
            var changeHandler = db.changes(since);
            changesRunning = true;
            changeHandler.onChange(drawItems);
        }
    }
    $.couchProfile.templates.profileReady = $("#new-message").html();
    $("#account").couchLogin({
        loggedIn : function(r) {
            $("#profile").couchProfile(r, {
                profileReady : function(profile) {
                    $("#create-message").submit(function(e){
                        e.preventDefault();
                        var form = this, doc = $(form).serializeObject();
                        doc.created_at = new Date();
                        doc.profile = profile;
                        db.saveDoc(doc, {success : function() {form.reset();}});
                        return false;
                    }).find("input").focus();
                }
            });
        },
        loggedOut : function() {
            $("#profile").html('<p>Please log in to see your profile.</p>');
        }
    });
 });