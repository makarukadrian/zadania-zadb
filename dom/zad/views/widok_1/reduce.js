function(keys, values, rereduce) {
  if (rereduce) {
    return {
      'suma': values.reduce(function(a, b) { return a + b.sum }, 0),
      'ilosc': values.reduce(function(a, b) { return a + b.count }, 0)
    }
  } else {
    return {
      'suma': sum(values),
      'ilosc': values.length
    }
  }
}